const { description } = require('../../package')

module.exports = {
  /**
   * Ref：https://v1.vuepress.vuejs.org/config/#title
   */
  // title: 'Cagn',
  /**
   * Ref：https://v1.vuepress.vuejs.org/config/#description
   */
  description: description,

  /**
   * Extra tags to be injected to the page HTML `<head>`
   *
   * ref：https://v1.vuepress.vuejs.org/config/#head
   */
  head: [
    ['meta', { name: 'theme-color', content: '#3eaf7c' }],
    ['meta', { name: 'apple-mobile-web-app-capable', content: 'yes' }],
    ['meta', { name: 'apple-mobile-web-app-status-bar-style', content: 'black' }],
    ['link', { rel: 'stylesheet', href: '/fonts/icomoon.css' }]
  ],

  /**
   * Theme configuration, here is the default theme configuration for VuePress.
   *
   * ref：https://v1.vuepress.vuejs.org/theme/default-theme-config.html
   */
  themeConfig: {
    repo: 'https://bitbucket.org/codeninelabs/cagn_docs/',
    logo: '/cagn_logo.png',
    editLinks: false,
    docsDir: '',
    editLinkText: '',
    lastUpdated: true,
    nav: [
      {
        text: 'Guide',
        link: '/guide/',
      },
      {
        text: 'Pub Dev',
        link: 'https://pub.dev/publishers/kevinandre.com/packages'
      }
    ],
    sidebarDepth: 2,
    displayAllHeaders: true,
    sidebar: [
        {
          title: 'Guide',
          path: '/guide/',
          collapsable: false,
          sidebarDepth: 0,
          children: [
            ['/guide/', 'Getting Started'],
            ['/guide/theming', 'Theming'],
            ['/guide/installation', 'Installation'],
          ]
        },
        {
          title: 'Widgets',
          path: '/widgets/',
          sidebarDepth: 2,
          collapsable: false,
          children: [
            ['/widgets/editors/', 'Editors'],
            {
              title: 'Popup',
              path: '/widgets/popup/',
              children: [
                ['/widgets/popup/installation', 'Installation']
              ],
            },
            {
              title: 'TreeView',
              path: '/widgets/treeview/',
              children: [
                ['/widgets/treeview/', 'Get Started'],
                ['/widgets/treeview/usage', 'Usage'],
                ['/widgets/treeview/controller', 'The Controller'],
                ['/widgets/treeview/theme', 'TreeView Theme'],
                ['/widgets/treeview/data', 'Custom Data'],
                ['/widgets/treeview/builder', 'Builder']
              ],
            },
          ]
        }
      ],
    },
  /**
   * Apply plugins，ref：https://v1.vuepress.vuejs.org/zh/plugin/
   */
  plugins: [
    '@vuepress/plugin-back-to-top',
    '@vuepress/plugin-medium-zoom',
  ]
}
