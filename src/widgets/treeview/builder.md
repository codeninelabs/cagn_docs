# TreeView Custom Builder

There may be cases where the default display combined with theme data aren’t  enough to meet the needs of your app. In this scenario, the `nodeBuilder` property of the TreeView may be used. It will allow you to build your node display the way you want while continuing to provide the expansion icon. You can add indicators, notes, summary info, etc. 

The `nodeBuilder` property is a Function that has a `BuildContext` and `Node` instance as parameters. You can use these to build your node’s display.

![Sample node with icon, subtitle, and indicator](DraggedImage.png)

This can be created with the following code:
```dart
// this would be added to the tree
var node = Node<double>(
  key: 'notifications',
  label: 'Email notifications',
  data: 13,
);

TreeView(
  nodeBuilder: (context, node) {
    return ListTile(
      title: Text(node.label),
      subtitle: node.data > 0 ? Text('Unread: ${node.data}') : Container(),
      trailing: SizedBox(
        width: 20,
        child: Stack(
          children: [
            Icon(Icons.mail), 
            Positioned(
              top: 5, 
              right: 5, 
              child: Container(
                decoration: BoxDecoration(
                  color: node.data > 0 ? Colors.green : Colors.transparent,
                  shape: BoxShape.circle,
                ),
              ),
            ),
          ]
        ),
      ),
    );
  }
);
```
