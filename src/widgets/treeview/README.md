# Cagn TreeView
This widget is listed as `flutter_treeview` on pub.dev.

## Overview

The TreeView is hierarchical data widget for your flutter apps. It offers a number of options for customizing the appearance and handling user interaction. It also offers some convenience methods for importing data into the tree.

## Description
This TreeView widget can be used to display hierarchical data in your project. 
Examples are:
* directory structures
* organization charts
* nested screen relationships
* inheritance trees, etc.

This widget uses a controller to manage the data. This allows the user to manipulate the data outside of the Flutter widget  `build` method. The data must be a list of type `Node`. Each node can have its own children list of type `Node`, thus creating the nested structure. The appearance of the TreeView can be controlled using a [TreeViewTheme](/widgets/treeview/theme).

## Features

* Separately customize child and parent labels
* Add any icon to a node
* Build node display with default or custom builder
* Attach data object to each tree node
* Choose from different expander icons and several modifiers for adjusting shape, outline, and fill. 
* Import data from a Map 
* Includes ability to handle expandChange, tap, and double tap user interactions
* Includes convenience methods for adding, updating and deleting nodes


## Usage
To use this package, add `flutter_treeview` as a dependency in your pubspec.yaml file.


## API Reference
The complete API reference can be found [here](#)(https://pub.dev/documentation/flutter_treeview/latest/).