# Controlling the TreeView
The TreeView uses a controller to manage the data. This allows the user to manipulate the data outside of the Flutter widget `build` method.

### Controller Class
The `TreeViewController` class defines the controller needed to display the nodes in the `TreeView`. The controller has two properties: `children` and `selectedKey`.

## Handle Node Tap
In most use cases, you will want to perform an action when a user taps a node. The `TreeView` class provides the `onNodeTap` property to allow you to perform an action when a node is tapped. The Function recieves the key of the node tapped. You can then use the `TreeViewController` to retrieve the actual node instance using the `getNode` method.

## Expanding Nodes
Each node controls its own expanded state. When a user expands a node, the `onExpansionChanged` method on the `TreeView` is called. You should use these method to change the state of the expanded node so that tree display is appropriately updated.

```dart
TreeView(
	onExpansionChanged: (key, state) {
	    Node node = _treeViewController.getNode(key);
	    if (node != null) {
	      List<Node> updated = _treeViewController.updateNode(key, node.copyWith(expanded: expanded));
	      setState(() {
	        _treeViewController = _treeViewController.copyWith(children: updated);
	      });
	    }
	}
);
```

This can also be useful if you want to conditionally expand the node or if you want to load tree nodes only when the user expands the node. This could is especially useful for large datasets.

#### Updating the TreeView
In order to update the display of the TreeView, the children have to be updated. This should happen using whatever state management mechanism you have in place. Unless you are completely replacing the entire children list, adding, updating, and deleting nodes can involve a number of steps. The TreeViewController tries to make this process simple by providing a number of methods that perform these steps in one method call.

For example, to update a specific node, you would have to perform these steps:
1. Retrieve the node by key
2. Update the properties of the node
3. Retrieve the list of nodes from the controller
4. Recursively propagate through the nodes to update the node as specified by the key
5. Get the new list of nodes to be saved to the controller

The `updateNode` method on the controller performs all of these steps with one call. It returns an updated list of nodes that you can then save to the controller.

Each similar method has a corresponding method that returns an updated controller vs a list of nodes. In the case of `updateNode` there is `withUpdateNode`. 

The `TreeViewController` has the following convenience methods: `addNode`, `collapseAll`, `collapseToNode`, `deleteNode`, `expandAll`, `expandToNode`, `getNode`, `getParent`, `loadJSON`, `loadMap`, `toggleNode`, `updateAll`, `withAddNode`, `withCollapseAll`, `withCollapseToNode`, `withDeleteNode`, `withExpandAll`, `withExpandToNode`, `getNode`, `getParent`, `loadJSON`, `loadMap`, `withToggleNode`, `withUpdateAll`. 

Use the API reference to learn more about available methods.

#### API Reference
View the `TreeViewController` [properties and methods here](https://pub.dev/documentation/flutter_treeview/latest/flutter_treeview/TreeViewController-class.html).