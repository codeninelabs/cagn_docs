# TreeView Theme

The `TreeViewTheme` class allows you to customize the appearance of the `TreeView`. It provides properties that can control the display of the parent and child text, the expander icon, icon color and size, expansion speed, and more. These properties can be static and initialized once at the start of your app or be dynamic and set to met the changing needs of your application.

The TreeViewTheme has the following properties:
* **expanderTheme** - the appearance theme for TreeNode expander icons.
* **labelStyle** - the text style for child TreeNode text.
* **labelOverflow** - the text overflow for child TreeNode text.  
	_If this property is null then softWrap is true;_
* **parentLabelStyle** - the text style for parent TreeNode text.
* **parentLabelOverflow** - the text overflow for parent TreeNode text.   
	_If this property is null then softWrap is true;_
* **iconTheme** - the default appearance theme for TreeNode icons.
* **iconPadding** - horizontal padding for node icons.
* **colorScheme** - the color scheme to use for the widget.
* **expandSpeed** - the speed at which expander icon animates.
* **dense** - determines whether the TreeView is vertically dense.
* **horizontalSpacing** - horizontal spacing between tabs.   
	_If this property is null then horizontal spacing between tabs is default \_treeView.theme.iconTheme.size + 5_
* **verticalSpacing** - vertical spacing between tabs.   
	_If this property is null then dense attribute will work and vice versa._

## ExpanderTheme
The expander theme allows you change the appearance of the icon used to expand or collapse a tree node.

<expander />

## Example
```dart
/// sample theme
TreeViewTheme treeViewTheme = TreeViewTheme(
  expanderTheme: ExpanderThemeData(
    type: ExpanderType.caret,
    modifier: ExpanderModifier.none,
    position: ExpanderPosition.start,
    color: Colors.red.shade800,
    size: 20,
  ),
  labelStyle: TextStyle(
    fontSize: 16,
    letterSpacing: 0.3,
  ),
  parentLabelStyle: TextStyle(
    fontSize: 16,
    letterSpacing: 0.1,
    fontWeight: FontWeight.w800,
    color: Colors.red.shade600,
  ),
  iconTheme: IconThemeData(
    size: 18,
    color: Colors.grey.shade800,
  ),
  colorScheme: ColorScheme.light(),
);
```
