---
sidebar: auto
---
# Installation

## Browser API Access Restrictions

If you are using or demoing components that are not SSR friendly (for example containing custom directives), you can wrap them inside the built-in `<ClientOnly>` component:

##
