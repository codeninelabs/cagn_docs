# Getting Started

Cagn is a collection of widgets and utilities for use in developing Flutter apps. It consists of a [TreeView Widget](/widgets/treeview/), a [Popup Widget](/widgets/popup/), and a variety of input widgets called [Editors](/widgets/editors/).

Each widget comes with it's own theme data class that can be used to style the display. Each themedata class should contain everything needed to style everyaspect of the widget. In some cases, the widget styling falls back on the overall theme data provided to your app. If there are cases where more granularity is needed, the widgets allow a builder for creating the display contents the way you prefer.

This guide provides details on the general usage of the widgets. There are also examples of the widgets being used in the variety of scenarios. 