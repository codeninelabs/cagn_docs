# Installation

### Depend on package
Run this command with [Flutter](https://flutter.io):

#### TreeView
``` sh
$ flutter pub add flutter_treeview
```
#### Popup
``` sh
$ flutter pub add cagn_popup
```
#### Editors
``` sh
$ flutter pub add cagn_editors
```


This will add a line like this to your package's pubspec.yaml (and run an implicit `flutter pub get`):

```yaml
    dependencies:
        flutter_treeview: 
        cagn_popup: 
        cagn_editors: 
```

You can also manually add the dependencies as above and then explicitly run `flutter pub get`. Some IDEs will automatically execute this command for you when you add the dependency and then save.

### Import it
Now in your dart code, you can use:
```dart
/// for TreeView
import 'package:flutter_treeview/flutter_treeview.dart';

/// for Popup
import 'package:cagn_popup/cagn_popup.dart';

/// for Editors
import 'package:cagn_editors/cagn_editors.dart';
```